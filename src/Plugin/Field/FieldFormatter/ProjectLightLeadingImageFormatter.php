<?php

namespace Drupal\pl9settings\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of project_light_leading_image_formatter.
 *
 * @FieldFormatter(
 *   id = "project_light_leading_image_formatter",
 *   label = @Translation("Project Light Leading Image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ProjectLightLeadingImageFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    $url = NULL;
    $image_link_setting = $this->getSetting('image_link');
    // Check if the formatter involves a link.
    if ($image_link_setting == 'content') {
      $entity = $items->getEntity();
      if (!$entity->isNew()) {
        $url = $entity->toUrl();
      }
    }
    elseif ($image_link_setting == 'file') {
      $link_file = TRUE;
    }

    $image_style_setting = $this->getSetting('image_style');

    // Collect cache tags to be added for each item in the field.
    $base_cache_tags = [];
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $base_cache_tags = $image_style->getCacheTags();
    }

    foreach ($files as $delta => $file) {
      $image_uri = $file->getFileUri();
      if (isset($link_file)) {
        $url = $this->fileUrlGenerator->generate($image_uri);
      }
      $cache_tags = Cache::mergeTags($base_cache_tags, $file->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $image_loading_settings = $this->getSetting('image_loading');
      $item_attributes['loading'] = $image_loading_settings['attribute'];

      // Without transformRelative() we get http (not https) URLs. The Drupal
      // recommendation seems to be to stick with relative URLs in order to
      // avoid mixed-mode problems.
      $item_attributes['data-src-12'] = $this->fileUrlGenerator->transformRelative(ImageStyle::load('leading_1177')->buildUrl($image_uri));
      $item_attributes['data-src-9'] = $this->fileUrlGenerator->transformRelative(ImageStyle::load('leading_885')->buildUrl($image_uri));
      $item_attributes['data-src-6'] = $this->fileUrlGenerator->transformRelative(ImageStyle::load('leading_590')->buildUrl($image_uri));

      $elements[$delta] = [
        '#theme' => 'image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => $image_style_setting,
        '#url' => $url,
        '#cache' => [
          'tags' => $cache_tags,
        ],
      ];
    }

    return $elements;
  }

}
